let config = {
  name: 'AES-GCM', length: 128
};
let keyUsages = ['encrypt', 'decrypt'];
let key = await crypto.subtle.generateKey(config,
  false, keyUsages);
let iv = await crypto.getRandomValues(new Uint8Array(12));
let te = new TextEncoder();
let ad = te.encode("some associated data"); let plaintext = te.encode("hello world");
let param = {
  name: 'AES-GCM', iv: iv, additionalData: ad
};
let ciphertext = await crypto.subtle.encrypt(param,
  key, plaintext);
let result = await window.crypto.subtle.decrypt(param, key, ciphertext);
new TextDecoder("utf-8").decode(result);