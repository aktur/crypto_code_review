package main

import (
	"crypto/tls"
	"log"
	"net/http"
)

func main() {
	config := &tls.Config{
		MinVersion: tls.VersionTLS13}
	http.HandleFunc("/", func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("Hello, world\n"))
	})
	server := &http.Server{Addr: ":8080", TLSConfig: config}
	err := server.ListenAndServeTLS("cert.pem", "key.pem")
	if err != nil {
		log.Fatal(err)
	}
}
