from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey


def main():
    private_key = Ed25519PrivateKey.generate()
    public_key = private_key.public_key()
    message = b"example.com has the public key 0xab70..."
    signature = private_key.sign(message)
    try:
        public_key.verify(signature, message)
        print("valid signature")
    except InvalidSignature:
        print("invalid signature")


if __name__ == "__main__":
    exit(main())
