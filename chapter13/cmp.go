package main

func ConstantTimeCompare(x, y []byte) byte {
	if len(x) != len(y) {
		return 0
	}
	var v byte
	for i := 0; i < len(x); i++ {
		v |= x[i] ^ y[i]
	}
	return v
}

func main(){
	var s string = "Hello World"
	var t string = "Hello Worla"
	var x []byte = []byte(s)
	var y []byte = []byte(t)
		print(ConstantTimeCompare(x, y))
}