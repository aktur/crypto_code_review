package com.dineshonjava;
import com.google.crypto.tink.HybridDecrypt;
import com.google.crypto.tink.HybridEncrypt;
import com.google.crypto.tink.hybrid.HybridKeyTemplates; 
import com.google.crypto.tink.KeysetHandle;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        byte[] plaintext = new byte[] { (byte)0xe0, 0x4f, (byte)0xd0,
            0x20, (byte)0xea, 0x3a, 0x69, 0x10, (byte)0xa2, (byte)0xd8, 0x08, 0x00, 0x2b,
            0x30, 0x30, (byte)0x9d };
        byte[] associatedData = new byte[] { (byte)0xe0, 0x4f, (byte)0xd0,
            0x20, (byte)0xea, 0x3a, 0x69, 0x10, (byte)0xa2, (byte)0xd8, 0x08, 0x00, 0x2b,
            0x30, 0x30, (byte)0x9d };
        KeysetHandle privateKeysetHandle = KeysetHandle.generateNew( HybridKeyTemplates.ECIES_P256_HKDF_HMAC_SHA256_AES128_GCM);
        KeysetHandle publicKeysetHandle = privateKeysetHandle.getPublicKeysetHandle();
        HybridEncrypt hybridEncrypt = publicKeysetHandle.getPrimitive(HybridEncrypt.class);
        byte[] ciphertext = hybridEncrypt.encrypt(plaintext, associatedData);
        HybridDecrypt hybridDecrypt = privateKeysetHandle.getPrimitive( HybridDecrypt.class);
        byte[] plaintext = hybridDecrypt.decrypt(ciphertext, associatedData);
              System.out.println( "Hello World!" );
    }
}
