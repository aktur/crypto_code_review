import com.google.crypto.tink.HybridDecrypt;
import com.google.crypto.tink.HybridEncrypt;
import com.google.crypto.tink.hybrid.HybridKeyTemplates; 
import com.google.crypto.tink.KeysetHandle;

public class hybrid_encryption {
  public static void main(String []args) {
  KeysetHandle privateKeysetHandle = KeysetHandle.generateNew( HybridKeyTemplates.ECIES_P256_HKDF_HMAC_SHA256_AES128_GCM);
  KeysetHandle publicKeysetHandle = privateKeysetHandle.getPublicKeysetHandle();
  HybridEncrypt hybridEncrypt = publicKeysetHandle.getPrimitive(HybridEncrypt.class);
  byte[] ciphertext = hybridEncrypt.encrypt(plaintext, associatedData);
  HybridDecrypt hybridDecrypt = privateKeysetHandle.getPrimitive( HybridDecrypt.class);
  byte[] plaintext = hybridDecrypt.decrypt(ciphertext, associatedData);
  }

}