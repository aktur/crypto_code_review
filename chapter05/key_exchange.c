unsigned char client_pk[crypto_kx_PUBLICKEYBYTES];
unsigned char client_sk[crypto_kx_SECRETKEYBYTES];
crypto_kx_keypair(client_pk, client_sk);
unsigned char server_pk[crypto_kx_PUBLICKEYBYTES];
obtain(server_pk);
unsigned char key_to_decrypt[crypto_kx_SESSIONKEYBYTES];
unsigned char key_to_encrypt[crypto_kx_SESSIONKEYBYTES];
if (crypto_kx_client_session_keys(key_to_decrypt, key_to_encrypt, client_pk, client_sk, server_pk) != 0)
{
  abort_session();
}