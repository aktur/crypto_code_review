[Timesheet](report_2021-04-01_2021-04-30.pdf)

# Chapter 2

## page 30

book
```
$ openssl dgst -sha256 downloaded_file
f63e68ac0bf052ae923c03f5b12aedc6cca49874c1c9b0ccf3f39b662d1f487b
```

check (different file) shows **_additional output_** beside a hash
```
$ openssl dgst -sha256 crypto_review.md
SHA256(crypto_review.md)= 2ea616078db6a63f9ddfe1c06f03022cc858a5f90fc0d9322eb1b69d4dae581b
```

## page 31

book
```
$ echo -n "hello" | openssl dgst -sha256
2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824
$ echo -n "hello" | openssl dgst -sha256
2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824
$ echo -n "hella" | openssl dgst -sha256
70de66401b1399d79b843521ee726dcec1e9a8cb5708ec1520f1f3bb4b1dd984
$ echo -n "this is a very very very very very very very very very very very long sentence" | openssl dgst -sha256
009e286e0261a8d0eca95649cf795db3572c515fe9dc7e319ece5af8f133637a
```

check **OK**

## page 40

book
```
$ echo -n "hello world" | openssl dgst -sha224 
2f05477fc24bb4faefd86517156dafdecec45b8ad3cf2522a563582b
$ echo -n "hello world" | openssl dgst -sha256 
b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9 
$ echo -n "hello world" | openssl dgst -sha384 
fdbd8e75a67f29f701a4e040385e2e23986303ea10239211af907fcbb83578b3e 417cb71ce646efd0819dd8c088de1bd
$ echo -n "hello world" | openssl dgst -sha512 
309ecc489c12d6eb4cc40f50c902f2b4d0ed77ee511a7c7a9bcd3ca86d4cd86f9 89dd35bc5ff499670da34255b45b0cfd830e81f605dcf7dc5542e93ae9cd76f
```

check **OK**

## page 51

book
```
$ echo -n "Alice""Bob""100""15" | openssl dgst -sha3-256 
34d6b397c7f2e8a303fc8e39d283771c0397dad74cef08376e27483efc29bb02
```

On macOS (Catalina 10.16.7) gives an error:
**unknown option '-sha3-256'**

works on Ubuntu with slighty different output
```
$ echo -n "Alice""Bob""100""15" | openssl dgst -sha3-256
(stdin)= 34d6b397c7f2e8a303fc8e39d283771c0397dad74cef08376e27483efc29bb02
```

# Chapter 3

It is in Rust and later Go. To test it packages need to be initiated and extra source build.

## page 60

**Code does not build!**

I was trying `cargo build` and got the following:
```
   Compiling chapter03 v0.1.0 (/Users/michal/Developer/projects/learn/manning/crypto_source_review/chapter03)
error[E0432]: unresolved import `hmac`
 --> src/main.rs:1:5
  |
1 | use hmac::{Hmac, Mac, NewMac};
  |     ^^^^ use of undeclared type or module `hmac`

error[E0432]: unresolved import `sha2`
 --> src/main.rs:2:5
  |
2 | use sha2::Sha256;
  |     ^^^^ use of undeclared type or module `sha2`

error: aborting due to 2 previous errors

For more information about this error, try `rustc --explain E0432`.
error: could not compile `chapter03`.

```

## page 67

- example misses variables declaration therefore it does not compile.
- filename seems to have double .go.go extension, does not seem right

```
go run constant_time.go
# command-line-arguments
./constant_time.go:7:22: undefined: x
./constant_time.go:8:3: undefined: v
./constant_time.go:8:8: undefined: x
./constant_time.go:8:15: undefined: y
```

# Chapter 4

It has JavaScript, not Go!

## page 89 listing 4.1

**Code does not compile**

```
node encrypt.js 
/Users/michal/Developer/projects/learn/manning/crypto_source_review/chapter04/encrypt.js:5
let key = await crypto.subtle.generateKey(config,
          ^^^^^

SyntaxError: await is only valid in async function
    at wrapSafe (internal/modules/cjs/loader.js:979:16)
    at Module._compile (internal/modules/cjs/loader.js:1027:27)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1092:10)
    at Module.load (internal/modules/cjs/loader.js:928:32)
    at Function.Module._load (internal/modules/cjs/loader.js:769:14)
    at Function.executeUserEntryPoint [as runMain] (internal/modules/run_main.js:72:12)
    at internal/main/run_main_module.js:17:47
```

# Chapter 5

## page 105

C code is not valid and cannot be run

# Chapter 6

## page 138

Getting this file to compile without any support files for pom, gradle or whatever costs too much time, the example should contain all necessary code. I've created dummy HelloWorld Maven example just to get it through compiler.

This code is wrong, plaintext variable is used before declaration, which results in compilation error.

# Chapter 7

## page 155

OK

# Chapter 8

## page 188

shell OK

C code does not compile
```
> cc random_numbers.c
In file included from random_numbers.c:1:
/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/random.h:36:21: error: expected parameter declarator
    __OSX_AVAILABLE(10.12) __IOS_AVAILABLE(10.0) __TVOS_AVAILABLE(10.0) __WATCHOS_AVAILABLE(3.0)
                    ^
/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/random.h:36:21: error: expected ')'
/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/random.h:36:20: note: to match this '('
    __OSX_AVAILABLE(10.12) __IOS_AVAILABLE(10.0) __TVOS_AVAILABLE(10.0) __WATCHOS_AVAILABLE(3.0)
                   ^
/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/random.h:36:28: error: expected function body after function declarator
    __OSX_AVAILABLE(10.12) __IOS_AVAILABLE(10.0) __TVOS_AVAILABLE(10.0) __WATCHOS_AVAILABLE(3.0)
                           ^
random_numbers.c:2:1: error: unknown type name 'uint8_t'
uint8_t secret[16];
^
random_numbers.c:3:16: error: implicit declaration of function 'getrandom' is invalid in C99 [-Werror,-Wimplicit-function-declaration]
int len_read = getrandom(secret, sizeof(secret), 0);
               ^
random_numbers.c:4:1: error: expected identifier or '('
if (len_read != sizeof(secret)) { abort();
^
6 errors generated.
```

## page 189

PHP OK

# Chapter 9

## page 209

tls_server.go OK

The second listing does not work because cert.pem and key.pem files are missing.

## page 221

I don't know how to check ASN.1 code :-(

## page 223

There is no description how to fetch google.pem. Without this file the example bash fails. Moreover, the example shows very old certificate which is already outdated.

How to get cert:
```
openssl s_client -showcerts -connect google.com:443 </dev/null 2>/dev/null|openssl x509 -outform PEM >google.pem
```

# Chapter 13

## page 343

Go code OK

# Chapter 15

## page 397

The code is in not defined language. Is it Rust?