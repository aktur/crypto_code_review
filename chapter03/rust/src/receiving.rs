use sha2::Sha256;
use hmac::{Hmac, Mac, NewMac};
fn receive_message(key: &[u8], message: &[u8], authentication_tag: &[u8]) -> bool {
let mut mac = Hmac::<Sha256>::new(key); mac.update(message);
mac.verify(&authentication_tag).is_ok() a}