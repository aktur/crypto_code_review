use hmac::{Hmac, Mac, NewMac};
use sha2::Sha256;
fn send_message(key: &[u8], message: &[u8]) -> Vec<u8> {
    let mut mac = Hmac::<Sha256>::new(key.into());
    mac.update(message);
    mac.finalize().into_bytes().to_vec()
}

fn main() {
    println!("Hello, world!");
}
